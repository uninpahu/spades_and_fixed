(function(){
  'use strict';
  angular.module('spades')
  .config(['$stateProvider','$urlRouterProvider',function($stateProvider,$urlRouterProvider) {
  	$urlRouterProvider.otherwise("/init");
    $stateProvider
    .state('app', {
      abstract: true,
      template: '<div class="page-container"><div ui-view class="ng-fadeInLeftShort"></div></div>',
      controller: 'SpadesAndFixedController',
    })

    .state('app.init', {
      url: '/init',
      title: 'Spades and Fixed',
      templateUrl: 'views/spades_and_fixed/index.html',
    })
    .state('app.spades_and_fixed', {
      url: '/spades_and_fixed',
      title: 'Spades and Fixed',
      templateUrl: 'views/spades_and_fixed/game.html',
    })
    ;
  }]);
})();