(function(){
  'use strict';
  angular.module('spades')
  .controller('SpadesAndFixedController',['$scope','$state',function($scope,$state){
    $scope.data 					= {};
    $scope.data.user_name = "";
    $scope.numbers				= [];
		$scope.number         = "";
    $scope.alerts         = [];
    $scope.number_secret_button = false;
    $scope.attempts       = [];

    $scope.start_game = function(){
    	$scope.number        = number_generate();
      $scope.secret_number = "****";
    	if ($scope.data.user_name !== "") {
    		$state.go("app.spades_and_fixed");
    	}else{
        $scope.alerts['user'] = {
          status: true,
          message: "Usuario no debe ser vacio."
        }
    	}	
    }

    $scope.number_secret_view = function(event){
      if (event === 'show') {
        $scope.number_secret_button = true;
        $scope.secret_number        = $scope.number;
      }else if (event === 'hide') {
        $scope.number_secret_button = false;
        $scope.secret_number        = "****";
      }
    }

    $scope.start_compare = function(){
    	console.log($scope.data.compare);
    	if ($scope.data.compare !== undefined) {
    		var compares = $scope.data.compare.split("");
    		var numbers  = $scope.number.split("");
    		console.info(compares);
    		console.info(numbers);

    		var picas = 0;
    		var fijas = 0;
    		angular.forEach(compares, function(val_compare, key_compare){	
    			angular.forEach(numbers, function(val_real, key_real){
  					if (val_compare === val_real) {
    					if (key_compare === key_real) {
    						fijas += 1;
    					}else{
    						picas += 1;
    					}
    				}
    			});
    		});

        if (fijas < 4) {
          $scope.attempts[$scope.attempts.length] = $scope.data.compare+"  =>  "+"   Picas "+picas+"    Fijas "+fijas;
        }else{
          $scope.attempts[$scope.attempts.length] = "Has acertado, en "+ ($scope.attempts.length+1) +" intentos";
        }
        $scope.data.compare = "";
    	}else{
    		console.info('el dato a comparar no debe estar vacio');
    	}

    }

		//private
    var number_generate =  function(){
    	var total_digits = 4;
    	$scope.numbers 	 = [];
    	for(var i = 0; i < total_digits; i++){
    		$scope.numbers[$scope.numbers.length] = number_create($scope.numbers);
  		}
  		return $scope.numbers.join("");
    }

    var number_create = function(array_excepts){
    	var number = null;
    	if (array_excepts.length !== 0) {
				var number_exists = true;

				while(number_exists){
					number_exists = false;
					number 				= get_random();
					angular.forEach(array_excepts, function(val){
						if (!number_exists) {
			  			if (number === val) {
			  				number_exists = true;
			  			}
						}
		  		});
				}
  			return number;
    	}
    	number = get_random();
    	return number;
    }

    var get_random = function () {
    	return Math.floor((Math.random() * 9));
    }

  }]);
})();